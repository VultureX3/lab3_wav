#pragma once

#include <exception>
#include <stdexcept>

class WavException : public std::exception
{
public:
	WavException(const std::string &message);
	~WavException() noexcept;


	const char* what() const;

private:
	std::string message;
};

class IO_ERROR : public WavException
{
public:
	IO_ERROR(const std::string &message);
};

class BAD_FORMAT : public WavException
{
public:
	BAD_FORMAT(const std::string &message);
};

class UNSUPPORTED_FORMAT : public WavException
{
public:
	UNSUPPORTED_FORMAT(const std::string &message);
};

class BAD_PARAMS : public WavException
{
public:
	BAD_PARAMS(const std::string &message);
};

class DATA_SIZE_ERROR : public WavException
{
public:
	DATA_SIZE_ERROR(const std::string &message);
};

class HEADER_RIFF_ERROR : public WavException
{
public:
	HEADER_RIFF_ERROR(const std::string &message);
};

class HEADER_FILE_SIZE_ERROR : public WavException
{
public:
	HEADER_FILE_SIZE_ERROR(const std::string &message);
};

class HEADER_WAVE_ERROR : public WavException
{
public:
	HEADER_WAVE_ERROR(const std::string &message);
};

class HEADER_FMT_ERROR : public WavException
{
public:
	HEADER_FMT_ERROR(const std::string &message);
};

class HEADER_NOT_PCM : public WavException
{
public:
	HEADER_NOT_PCM(const std::string &message);
};

class HEADER_SUBCHUNK1_ERROR : public WavException
{
public:
	HEADER_SUBCHUNK1_ERROR(const std::string &message);
};

class HEADER_BYTES_RATE_ERROR : public WavException
{
public:
	HEADER_BYTES_RATE_ERROR(const std::string &message);
};

class HEADER_BLOCK_ALIGN_ERROR : public WavException
{
public:
	HEADER_BLOCK_ALIGN_ERROR(const std::string &message);
};

class HEADER_SUBCHUNK2_SIZE_ERROR : public WavException
{
public:
	HEADER_SUBCHUNK2_SIZE_ERROR(const std::string &message);
};