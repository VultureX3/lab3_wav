#pragma warning(disable : 4996)

#include <cstdio>
#include <cstring>

#include "wav_header.h"
#include "wav_core.h"
#include "wav_exceptions.h"


// TODO: Remove all 'magic' numbers
// TODO: Make the code more secure. Get rid of pointers (after creating a class, of course).


void WavData::ReadHeader(const char *filename)
{
    printf( ">>>> ReadHeader( %s )\n", filename );
    NullHeader(); // Fill header with zeroes.

    FILE* f = fopen( filename, "rb" );
    if ( !f ) {
        throw IO_ERROR("IO_ERROR");
    }

    size_t blocks_read = fread( &WavHeader, sizeof(wav_header_s), 1, f);
    if ( blocks_read != 1 ) {
        // can't read header, because the file is too small.
        throw BAD_FORMAT("BAD_FORMAT");
    }

    fseek( f, 0, SEEK_END ); // seek to the end of the file
    size_t file_size = ftell( f ); // current position is a file size!
    fclose( f );

	CheckHeader(file_size);
}

void WavData::PrintInfo()
{
    printf( "-------------------------\n" );
    printf( " audioFormat   %u\n", WavHeader.audioFormat );
    printf( " numChannels   %u\n", WavHeader.numChannels );
    printf( " sampleRate    %u\n", WavHeader.sampleRate );
    printf( " bitsPerSample %u\n", WavHeader.bitsPerSample );
    printf( " byteRate      %u\n", WavHeader.byteRate );
    printf( " blockAlign    %u\n", WavHeader.blockAlign );
    printf( " chunkSize     %u\n", WavHeader.chunkSize );
    printf( " subchunk1Size %u\n", WavHeader.subchunk1Size );
    printf( " subchunk2Size %u\n", WavHeader.subchunk2Size );
    printf( "-------------------------\n" );
}


void WavData::CreateFromFile( const char* filename, std::vector<std::vector<short>>& channels_data )
{
    printf( ">>>> CreateFromFile( %s )\n", filename );
    ReadHeader( filename );

    if ( WavHeader.bitsPerSample != 16 ) {
        // Only 16-bit samples is supported.
        throw UNSUPPORTED_FORMAT("UNSUPPORTED_FORMAT");
    }

    FILE* f = fopen( filename, "rb" );
    if ( !f ) {
		throw IO_ERROR("IO_ERROR");
    }
    fseek( f, 44, SEEK_SET ); // Seek to the begining of PCM data.

    int chan_count = WavHeader.numChannels;
    int samples_per_chan = (WavHeader.subchunk2Size / sizeof(short) ) / chan_count;

    // 1. Reading all PCM data from file to a single vector.
    std::vector<short> all_channels;
    all_channels.resize( chan_count * samples_per_chan );
    size_t read_bytes = fread( all_channels.data(), 1, WavHeader.subchunk2Size, f );
    if ( read_bytes != WavHeader.subchunk2Size ) {
        printf( "CreateFromFile() read only %zu of %u\n", read_bytes, WavHeader.subchunk2Size );
        throw IO_ERROR("IO_ERROR");
    }
    fclose( f );


    // 2. Put all channels to its own vector.
    channels_data.resize( chan_count );
    for ( size_t ch = 0; ch < channels_data.size(); ch++ ) {
        channels_data[ ch ].resize( samples_per_chan );
    }

    for ( int ch = 0; ch < chan_count; ch++ ) {
        std::vector<short>& chdata = channels_data[ ch ];
        for ( size_t i = 0; i != samples_per_chan; i++ ) {
            chdata[ i ] = all_channels[ chan_count * i + ch ];
        }
    }
}


void WavData::CheckHeader(size_t file_size_bytes )
{
    // Go to wav_header.h for details

    if ( WavHeader.chunkId[0] != 'R' ||
         WavHeader.chunkId[1] != 'I' ||
         WavHeader.chunkId[2] != 'F' ||
         WavHeader.chunkId[3] != 'F' )
    {
        printf( "HEADER_RIFF_ERROR\n" );
        throw HEADER_RIFF_ERROR("HEADER_RIFF_ERROR");
    }

    if ( WavHeader.chunkSize != file_size_bytes - 8 ) {
        printf( "HEADER_FILE_SIZE_ERROR\n" );
        throw HEADER_FILE_SIZE_ERROR ("HEADER_FILE_SIZE_ERROR");
    }

    if ( WavHeader.format[0] != 'W' ||
         WavHeader.format[1] != 'A' ||
         WavHeader.format[2] != 'V' ||
         WavHeader.format[3] != 'E' )
    {
        printf( "HEADER_WAVE_ERROR\n" );
        throw HEADER_WAVE_ERROR("HEADER_WAVE_ERROR");
    }

    if ( WavHeader.subchunk1Id[0] != 'f' ||
         WavHeader.subchunk1Id[1] != 'm' ||
         WavHeader.subchunk1Id[2] != 't' ||
         WavHeader.subchunk1Id[3] != ' ' )
    {
        printf( "HEADER_FMT_ERROR\n" );
        throw HEADER_FMT_ERROR("HEADER_FMT_ERROR");
    }

    if ( WavHeader.audioFormat != 1 ) {
        printf( "HEADER_NOT_PCM\n" );
        throw HEADER_NOT_PCM("HEADER_NOT_PCM");
    }

    if ( WavHeader.subchunk1Size != 16 ) {
        printf( "HEADER_SUBCHUNK1_ERROR\n" );
        throw HEADER_SUBCHUNK1_ERROR("HEADER_SUBCHUNK1_ERROR");
    }

    if ( WavHeader.byteRate != WavHeader.sampleRate * WavHeader.numChannels * WavHeader.bitsPerSample/8 ) {
        printf( "HEADER_BYTES_RATE_ERROR\n" );
        throw HEADER_BYTES_RATE_ERROR("HEADER_BYTES_RATE_ERROR");
    }

    if ( WavHeader.blockAlign != WavHeader.numChannels * WavHeader.bitsPerSample/8 ) {
        printf( "HEADER_BLOCK_ALIGN_ERROR\n" );
        throw HEADER_BLOCK_ALIGN_ERROR("HEADER_BLOCK_ALIGN_ERROR");
    }

    if ( WavHeader.subchunk2Id[0] != 'd' ||
         WavHeader.subchunk2Id[1] != 'a' ||
         WavHeader.subchunk2Id[2] != 't' ||
         WavHeader.subchunk2Id[3] != 'a' )
    {
        printf( "HEADER_FMT_ERROR\n" );
        throw HEADER_FMT_ERROR("HEADER_FMT_ERROR");
    }

    if ( WavHeader.subchunk2Size != file_size_bytes - 44 )
    {
        printf( "HEADER_SUBCHUNK2_SIZE_ERROR\n" );
        throw HEADER_SUBCHUNK2_SIZE_ERROR("HEADER_SUBCHUNK2_SIZE_ERROR");
    }
}

void WavData::PrefillHeader()
{
    // Go to wav_header.h for details

    WavHeader.chunkId[0] = 'R';
    WavHeader.chunkId[1] = 'I';
    WavHeader.chunkId[2] = 'F';
    WavHeader.chunkId[3] = 'F';

    WavHeader.format[0] = 'W';
    WavHeader.format[1] = 'A';
    WavHeader.format[2] = 'V';
    WavHeader.format[3] = 'E';

    WavHeader.subchunk1Id[0] = 'f';
    WavHeader.subchunk1Id[1] = 'm';
    WavHeader.subchunk1Id[2] = 't';
    WavHeader.subchunk1Id[3] = ' ';

    WavHeader.subchunk2Id[0] = 'd';
    WavHeader.subchunk2Id[1] = 'a';
    WavHeader.subchunk2Id[2] = 't';
    WavHeader.subchunk2Id[3] = 'a';

    WavHeader.audioFormat   = 1;
    WavHeader.subchunk1Size = 16;
    WavHeader.bitsPerSample = 16;

}

void WavData::FillHeader(int chan_count, int bits_per_sample, int sample_rate, int samples_count_per_chan)
{
    if ( bits_per_sample != 16 ) {
        throw UNSUPPORTED_FORMAT("UNSUPPORTED_FORMAT");
    }

    if ( chan_count < 1 ) {
        throw BAD_PARAMS("BAD_PARAMS");
    }
    PrefillHeader();

    int file_size_bytes = 44 + chan_count * (bits_per_sample/8) * samples_count_per_chan;

    WavHeader.sampleRate    = sample_rate;
    WavHeader.numChannels   = chan_count;
    WavHeader.bitsPerSample = 16;

    WavHeader.chunkSize     = file_size_bytes - 8;
    WavHeader.subchunk2Size = file_size_bytes - 44;

    WavHeader.byteRate      = WavHeader.sampleRate * WavHeader.numChannels * WavHeader.bitsPerSample/8;
    WavHeader.blockAlign    = WavHeader.numChannels * WavHeader.bitsPerSample/8;
}

void WavData::CreateNewFile(const char* filename, int sample_rate, const std::vector< std::vector<short> > &channels_data)
{
    printf( ">>>> CreateNewFile( %s )\n", filename );
    wav_header_s header;

    int chan_count = (int)channels_data.size();

    if ( chan_count < 1 ) {
        throw BAD_PARAMS("BAD_PARAMS");
    }

    int samples_count_per_chan = (int)channels_data[0].size();

    // Verify that all channels have the same number of samples.
    for ( size_t ch = 0; ch != chan_count; ch++ ) {
        if ( channels_data[ ch ].size() != (size_t) samples_count_per_chan ) {
            throw BAD_PARAMS("BAD_PARAMS");
        }
    }

    FillHeader(chan_count, 16, sample_rate, samples_count_per_chan );

    std::vector<short> all_channels;
    all_channels.resize( chan_count * samples_count_per_chan );

    for ( int ch = 0; ch < chan_count; ch++ ) {
        const std::vector<short>& chdata = channels_data[ ch ];
        for ( size_t i = 0; i != samples_count_per_chan; i++ ) {
            all_channels[ chan_count * i + ch ] = chdata[ i ];
        }
    }

    FILE* f = fopen( filename, "wb" );
    fwrite( &header, sizeof(wav_header_s), 1, f );
    fwrite( all_channels.data(), sizeof(short), all_channels.size(), f );
    if ( !f ) {
        throw IO_ERROR("IO_ERROR");
    }

    fclose( f );
}

void WavData::NullHeader()
{
    memset( &WavHeader, 0, sizeof(wav_header_s) );
}

void WavData::ConvertStereoToMono(const std::vector<std::vector<short> > &source, std::vector< std::vector<short> > &dest_mono)
{
    int chan_count = (int)source.size();

    if ( chan_count != 2 ) {
        throw BAD_PARAMS("BAD_PARAMS");
    }

    int samples_count_per_chan = (int)source[0].size();

    // Verify that all channels have the same number of samples.
    for ( size_t ch = 0; ch != chan_count; ch++ ) {
        if ( source[ ch ].size() != (size_t) samples_count_per_chan ) {
            throw BAD_PARAMS("BAD_PARAMS");
        }
    }

    dest_mono.resize( 1 );
    std::vector<short>& mono = dest_mono[ 0 ];
    mono.resize( samples_count_per_chan );

    // Mono channel is an arithmetic mean of all (two) channels.
    for ( size_t i = 0; i != samples_count_per_chan; i++ ) {
        mono[ i ] = ( source[0][i] + source[1][i] ) / 2;
    }
}
