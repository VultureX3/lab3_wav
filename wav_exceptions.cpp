#include "wav_exceptions.h"

WavException::WavException(const std::string &message) : message(message) { }

WavException::~WavException() { }

const char* WavException::what() const
{
	return message.c_str();
}

IO_ERROR::IO_ERROR(const std::string &message) : WavException(message) { }

BAD_FORMAT::BAD_FORMAT(const std::string &message) : WavException(message) { }

UNSUPPORTED_FORMAT::UNSUPPORTED_FORMAT(const std::string &message) : WavException(message) { }

BAD_PARAMS::BAD_PARAMS(const std::string &message) : WavException(message) { }

DATA_SIZE_ERROR::DATA_SIZE_ERROR(const std::string &message) : WavException(message) { }

HEADER_RIFF_ERROR::HEADER_RIFF_ERROR(const std::string &message) : WavException(message) { }

HEADER_FILE_SIZE_ERROR::HEADER_FILE_SIZE_ERROR(const std::string &message) : WavException(message) { }

HEADER_WAVE_ERROR::HEADER_WAVE_ERROR(const std::string &message) : WavException(message) { }

HEADER_FMT_ERROR::HEADER_FMT_ERROR(const std::string &message) : WavException(message) { }

HEADER_NOT_PCM::HEADER_NOT_PCM(const std::string &message) : WavException(message) { }

HEADER_SUBCHUNK1_ERROR::HEADER_SUBCHUNK1_ERROR(const std::string &message) : WavException(message) { }

HEADER_BYTES_RATE_ERROR::HEADER_BYTES_RATE_ERROR(const std::string &message) : WavException(message) { }

HEADER_BLOCK_ALIGN_ERROR::HEADER_BLOCK_ALIGN_ERROR(const std::string &message) : WavException(message) { }

HEADER_SUBCHUNK2_SIZE_ERROR::HEADER_SUBCHUNK2_SIZE_ERROR(const std::string &message) : WavException(message) { }