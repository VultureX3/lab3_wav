#include <iostream>

#include "wav_core.h"
#include "wav_exceptions.h"

using namespace std;

int main(int argc, char *argv[])
{
	try {
		cout << "************** | WavCore | **************" << endl;


		// ################  Tests for WavCore  ################

		const char* input_fname = "input.wav";
		const char* output_fname = "output.wav";

		// wav_header_s header;

		WavData file;


		// #### Opening WAV file, checking header.
		/*err = file.ReadHeader( input_fname, &header );
		if ( err != WAV_OK ) {
			cerr << "ReadHeader() error: " << (int)err << endl;
			file.PrintInfo( &header );
			return err;
		}*/
		file.ReadHeader(input_fname);

		// #### Printing header.
		file.PrintInfo();


		// #### Reading PCM data from file.
		vector< vector<short> > chans_data;
		/*err = file.CreateFromFile( input_fname, chans_data );
		if ( err != WAV_OK ) {
			cerr << "extract_data_int16() error: " << (int)err << endl;
			return err;
		}
		cout << endl << "********************" << endl;*/
		file.CreateFromFile(input_fname, chans_data);

		// #### Make several changes to PCM data.

		// # Making signal mono from stereo.
		vector< vector<short> > edited_data;
		/*err = file.ConvertStereoToMono( chans_data, edited_data );
		if ( err != WAV_OK ) {
			cerr << "make_mono() error: " << (int)err << endl;
			return err;
		}*/
		file.ConvertStereoToMono(chans_data, edited_data);

		// #### Making new WAV file using edited PCM data.
		/*err = file.CreateNewFile( output_fname, 44100, edited_data );
		if ( err != WAV_OK ) {
			cerr << "make_wav_file() error: " << (int)err << endl;
			file.PrintInfo( &header );
			return err;
		}*/
		file.CreateNewFile(output_fname, 44100, edited_data);
		file.PrintInfo();

		// #### Reading the file just created to check its header corectness.
		/*err = file.ReadHeader( output_fname, &header );
		if ( err != WAV_OK ) {
			cerr << "ReadHeader() error: " << (int)err << endl;
			file.PrintInfo( &header );
			return err;
		}*/
		file.ReadHeader(output_fname);
		file.PrintInfo();
	}
	catch (WavException &e) {
		cout << e.what();
	}
    
    return 0;
}

